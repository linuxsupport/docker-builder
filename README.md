# CI for base Docker images

This project is used for the common CI of our Docker images:
* [cc7-base](https://gitlab.cern.ch/linuxsupport/cc7-base)
* [c8-base](https://gitlab.cern.ch/linuxsupport/c8-base)
* [cs8-base](https://gitlab.cern.ch/linuxsupport/cs8-base)
